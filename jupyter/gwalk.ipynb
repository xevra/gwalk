{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "201ccc4b-53dc-45d3-b6e1-db93d540e7ee",
   "metadata": {},
   "source": [
    "# Gravitational Wave Approximate LiKelihood (GWALK)\n",
    "\n",
    "Welcome to GWALK, a truncated Gaussian code with the original purpose of approximating gravitational-wave likelihood functions for the parameter estimation samples provided along with the major Gravitational-Wave Transient Catalog publications.\n",
    "\n",
    "We intend to provide an object structure for truncated Gaussians which allows for efficient evaluation, parameterization, normalization, optimization, and serialization.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "82a0555f-0d8f-4361-87e3-80b455394950",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import numpy\n",
    "import numpy as np\n",
    "\n",
    "# Declare a seed\n",
    "SEED = 42\n",
    "\n",
    "# Initialize a random state object\n",
    "RS = np.random.RandomState(SEED)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fce33902-b27c-4ca5-bbce-ef725b57131e",
   "metadata": {},
   "source": [
    "## Parameterization ##\n",
    "\n",
    "The $\\mu$, $\\sigma$ and corellation parameters of the Multivariate Normal distribution can be accounted for with fewer elements than their matrix counterparts. This is essential to avoiding the problem of overconstraining the parameters of the distribution.\n",
    "\n",
    "For each N-dimensional Gaussian, we store:\n",
    "- 1 Paramter for a normalization constant, denoted \"offset\" (for log scale, this is added to the pdf)\n",
    "- N Parameters for the location of the Gaussian ($\\mu$), denoted \"mu\"\n",
    "- N Parameters for the width of the Gaussian ($\\sigma$), denoted \"std\"\n",
    "- (N \\times N - 1)/2 Parameters for the corellation parameters of the Gaussian, denoted \"cor\"\n",
    "\n",
    "We also store a scale factor for each parameter, which is used in calculations to ortho-normalize the distribution. This solves a lot of issues that arise with eigenvalues in the scipy representation when coordinates have vastly different ranges (i.e. $\\sigma_{\\tilde{\\Lambda}} \\propto 1000$, and $\\sigma_{\\chi_{eff}} \\propto 0.1$)\n",
    "\n",
    "We provide compiled code which translates these parameters to and from the $\\mu$ and $\\Sigma$ matrix parameterizations very quickly and efficiently, and have developed tests to ensure that a given choice of parameters satisfies the constraints of the multivariate normal distribution.\n",
    "\n",
    "The object representation for the truncated Gaussian handles this for the user."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "3a1025cc-a1c9-4e79-80e6-a202f765d9c8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "All parameters:\n",
      "[0.  1.  2.  3.  0.1 0.5 0.3 0.  0.3 0.5]\n",
      "\n",
      "offset:\n",
      "[0.]\n",
      "X[0]\n",
      "0.0\n",
      "\n",
      "mu:\n",
      "[1. 2. 3.]\n",
      "X[1:N+1]:\n",
      "[1. 2. 3.]\n",
      "\n",
      "std:\n",
      "[0.1 0.5 0.3]\n",
      "X[N+1:2*N+1]:\n",
      "[0.1 0.5 0.3]\n",
      "\n",
      "cor:\n",
      "[[1.  0.  0.3]\n",
      " [0.  1.  0.5]\n",
      " [0.3 0.5 1. ]]\n",
      "X[2*N+1:]:\n",
      "[0.  0.3 0.5]\n"
     ]
    }
   ],
   "source": [
    "# imports\n",
    "from gwalk.multivariate_normal.decomposition import params_of_offset_mu_cov\n",
    "from gwalk.multivariate_normal.decomposition import cov_of_std_cor\n",
    "\n",
    "# Pick some offset\n",
    "offset = np.asarray([0.])\n",
    "# Pick some mu\n",
    "mu = np.asarray([1., 2., 3.])\n",
    "# Pick some standard deviations\n",
    "std = np.asarray([0.1, 0.5, 0.3])\n",
    "# Pick some corelations\n",
    "cor = np.asarray([[1., 0., 0.3], [0., 1., 0.5], [0.3, 0.5, 1.]])\n",
    "\n",
    "# Assemble a covariance matrix\n",
    "cov = cov_of_std_cor(std, cor)\n",
    "# Get your parameters\n",
    "X = params_of_offset_mu_cov(offset, mu, cov).flatten()\n",
    "\n",
    "print(\"All parameters:\")\n",
    "print(X)\n",
    "print()\n",
    "print(\"offset:\")\n",
    "print(offset)\n",
    "print(\"X[0]\")\n",
    "print(X[0])\n",
    "print()\n",
    "print(\"mu:\")\n",
    "print(mu)\n",
    "print(\"X[1:N+1]:\")\n",
    "print(X[1:mu.size+1])\n",
    "print()\n",
    "print(\"std:\")\n",
    "print(std)\n",
    "print(\"X[N+1:2*N+1]:\")\n",
    "print(X[mu.size+1:2*mu.size + 1])\n",
    "print()\n",
    "print(\"cor:\")\n",
    "print(cor)\n",
    "print(\"X[2*N+1:]:\")\n",
    "print(X[2*mu.size + 1:])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "ac591cbf-29e9-4a42-b7ab-0758712ca48c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "MV1 params:\n",
      "[0.  1.  2.  3.  0.1 0.5 0.3 0.  0.3 0.5]\n",
      "mu:\n",
      "[1. 2. 3.]\n",
      "MV2 params:\n",
      "[0.  1.  2.  3.  0.1 0.5 0.3 0.  0.3 0.5]\n"
     ]
    }
   ],
   "source": [
    "# Introduce the object\n",
    "from gwalk.multivariate_normal import MultivariateNormal\n",
    "\n",
    "# Instantiate the object one way\n",
    "MV1 = MultivariateNormal(X)\n",
    "print(\"MV1 params:\")\n",
    "print(MV1.params)\n",
    "print(\"mu:\")\n",
    "print(MV1.mu)\n",
    "\n",
    "# Instantiate the object another way\n",
    "MV2 = MultivariateNormal.from_properties(mu=mu,cov=cov)\n",
    "print(\"MV2 params:\")\n",
    "print(MV2.params)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "4ad8ee6a-4244-4971-a127-0292464255e7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "limits:\n",
      "[[  1.  30.]\n",
      " [-30.  30.]\n",
      " [-30.  30.]]\n",
      "MV.limits:\n",
      "[[  1.  30.]\n",
      " [-30.  30.]\n",
      " [-30.  30.]]\n"
     ]
    }
   ],
   "source": [
    "# Truncate this Gaussian!\n",
    "limits = np.asarray([[1., 30.0], [-30., 30.], [-30.0, 30.]])\n",
    "# Print limits\n",
    "print(\"limits:\")\n",
    "print(limits)\n",
    "# Set up a truncate Gaussian\n",
    "MV = MultivariateNormal(X, limits=limits)\n",
    "print(\"MV.limits:\")\n",
    "print(MV.limits)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7be5c9a7-6d84-4da5-845b-80d4176c79f5",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Evaluation ##\n",
    "\n",
    "We provide a function which is compiled in C, to efficiently evaluate the Gaussian PDF for many Gaussians at once on a consistent set of points. In order to do this with scipy's multivariate Gaussian, this would need to be done in a python loop for each Gaussian."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "6749e5dd-d135-4d07-8221-f82e306db01e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Likelihood:\n",
      "[1.60596556e-20 1.33458422e-03 5.08146369e-63 ... 1.32003607e-04\n",
      " 1.11880892e-80 2.74739578e-05]\n"
     ]
    }
   ],
   "source": [
    "# Evaluate a non-truncated Gaussian\n",
    "\n",
    "# Import things from gwalk\n",
    "from gwalk.multivariate_normal import pdf as gwalk_pdf\n",
    "\n",
    "# Number of points to evaluate\n",
    "ndim = mu.size\n",
    "npts = int(1e4)\n",
    "\n",
    "# Come up with some bogus data to evaluate the pdf on\n",
    "Y = RS.uniform(size=npts*ndim).reshape(npts, ndim)\n",
    "# These points will be between 0. and 3.\n",
    "Y = Y*3\n",
    "\n",
    "# Evaluate the pdf\n",
    "L1 = gwalk_pdf(mu, cov, Y, scale=None, log_scale=False)\n",
    "print(\"Likelihood:\")\n",
    "print(L1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "50dfdc0a-5665-400c-b20a-5d4661417fb4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Likelihood (object):\n",
      "[3.21193112e-20 0.00000000e+00 1.01629274e-62 ... 2.64007214e-04\n",
      " 2.23761783e-80 0.00000000e+00]\n"
     ]
    }
   ],
   "source": [
    "# Evaluate a truncated Gaussian\n",
    "L2 = MV.likelihood(Y, scale=None)\n",
    "print(\"Likelihood (object):\")\n",
    "print(L2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2a9a5e46-4eb9-47f1-a564-35f5ef2c9ca7",
   "metadata": {},
   "source": [
    "## Normalization ##\n",
    "\n",
    "Normalizing a truncated Gaussian is very easy, as the one-dimensional CDFs for each coordinate capture everything about the space required to re-normalize the distribution. We also allow the normalization offset to float freely if desired, to allow rescaling of the multivariate normal distribution to suit the needs of different problems"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "1d278250-d6d8-4ca1-80dd-133a756e1466",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Integral of enclosed region:\n",
      "[0.5]\n",
      "Likelihood (accounting for normalization):\n",
      "[3.21193112e-20 0.00000000e+00 1.01629274e-62 ... 2.64007214e-04\n",
      " 2.23761783e-80 0.00000000e+00]\n",
      "Scale factor:\n",
      "2.000000000000006\n"
     ]
    }
   ],
   "source": [
    "# Find out what fraction of our Gaussian is inside the limits\n",
    "print(\"Integral of enclosed region:\")\n",
    "print(MV.analytic_enclosed_integral())\n",
    "\n",
    "# Normalize the MultivariateNormal object\n",
    "MV.normalize()\n",
    "L3 = MV.likelihood(Y)\n",
    "print(\"Likelihood (accounting for normalization):\")\n",
    "print(L3)\n",
    "print(\"Scale factor:\")\n",
    "print(np.mean((L3/L1)[L3 != 0]))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4be5d36-603a-4ee9-bddd-d319ac4c7c41",
   "metadata": {},
   "source": [
    "## Optimization ##\n",
    "\n",
    "Once the issues ofreducing our parameters to the minimal set (as seen above) to describe a multivariate normal distribution, providing a check for the constraints of a multivariate normal distribution, providing a means of evaluating the distribution efficiently, and providing an analytic way to resolve normalization with truncation are solved, optimization is fairly straightforward.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "6a17c1d7-981e-4ebd-8b77-b065bf2af72a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "There's some print statements I need to remove.\n",
      "[0.54881306 0.71518899 0.6027638  0.54488327 0.42365458 0.64589403\n",
      " 0.43758695 0.44588515 0.48183223 0.19172186]\n",
      "[ True]\n",
      "[[1.10576961e+09 2.07108744e+09 4.50359963e+09]]\n",
      "ln(optimized error):\n",
      "[-14.62308264 -14.80218518 -14.67665835 -16.26862825 -15.31476706\n",
      " -16.31395211 -15.13963096 -13.51752047 -13.97419588 -13.72027351]\n"
     ]
    }
   ],
   "source": [
    "## Imports ##\n",
    "from gwalk.multivariate_normal import random_gauss_params\n",
    "from gwalk.optimize_likelihood_grid import optimize_likelihood_grid\n",
    "from gwalk.multivariate_normal import pdf as multivariate_normal_pdf\n",
    "from gwalk.multivariate_normal import params_of_offset_mu_cov\n",
    "from gwalk.multivariate_normal import mu_cov_of_params\n",
    "from gwalk.multivariate_normal import offset_of_params\n",
    "from gwalk.multivariate_normal import MultivariateNormal\n",
    "\n",
    "## Constants ##\n",
    "\n",
    "# Number of samples to draw from Gaussian\n",
    "N_RVS = int(1e6)\n",
    "# Number of points to select for grid\n",
    "NSELECT = 1000\n",
    "# Number of dimensions\n",
    "NDIM = 3\n",
    "# Seed\n",
    "SEED = 0\n",
    "# Limits of Gaussian\n",
    "LIMITS = np.asarray([\n",
    "                     [-7., 7.],\n",
    "                     [-7., 7.],\n",
    "                     [-7., 7.],\n",
    "                    ])\n",
    "# Fit the least squares of the residual\n",
    "objective = \"lstsq\"\n",
    "# Fit using scipy's SLSQP method --\n",
    "# note this is the only method I have tested which\n",
    "# works for bounds and constraints\n",
    "method = \"SLSQP\"\n",
    "\n",
    "## Setup ##\n",
    "# Generate an arbitrary Gaussian\n",
    "RS.seed(SEED)\n",
    "X0 = random_gauss_params(1, NDIM, rs=RS)\n",
    "# Get the mu and cov params\n",
    "mu, cov = mu_cov_of_params(X0)\n",
    "# flatten the parameters, so we know we are dealing with the parameters\n",
    "# of a single Gaussian\n",
    "X0 = X0.flatten()\n",
    "\n",
    "## Generate some samples ##\n",
    "\n",
    "# Generate uniform samples\n",
    "samples = (RS.uniform(size=(N_RVS,NDIM))*(LIMITS[:,1]-LIMITS[:,0])) + LIMITS[:,0]\n",
    "# Get pdf values\n",
    "values = multivariate_normal_pdf(mu, cov, samples, log_scale=True).flatten()\n",
    "# Adjust by the lnL offset\n",
    "values = values + offset_of_params(X0)\n",
    "# Downselect samples\n",
    "select = np.argsort(values)[-NSELECT:]\n",
    "# Keep only NSELECT samples\n",
    "samples = samples[select]\n",
    "values = values[select]\n",
    "\n",
    "## Run the optimize likelihood grid function ##\n",
    "print(\"There's some print statements I need to remove.\")\n",
    "\n",
    "MV = optimize_likelihood_grid(\n",
    "                              samples,\n",
    "                              lnP=values,\n",
    "                              seed=SEED,\n",
    "                              objective=objective,\n",
    "                              method=method,\n",
    "                             )\n",
    "\n",
    "# Retrieve estimated value of parameters from optimization\n",
    "Xg = MV.params\n",
    "\n",
    "# Print the log of the absolute value of the difference between X0 and Xg\n",
    "print(\"ln(optimized error):\")\n",
    "print(np.log(np.abs(Xg-X0)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c303228c-7646-4986-9957-c8747430dacb",
   "metadata": {},
   "source": [
    "## Serialization ##\n",
    "\n",
    "We also provide a way to save and load these distributions in a compressed file format.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "da2909f6-7def-4d49-af37-5dc31d10d7a4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Testing serialization:\n",
      "Saved params:\n",
      "[0.54881306 0.71518899 0.6027638  0.54488327 0.42365458 0.64589403\n",
      " 0.43758695 0.44588515 0.48183223 0.19172186]\n",
      "Loaded params:\n",
      "[0.54881306 0.71518899 0.6027638  0.54488327 0.42365458 0.64589403\n",
      " 0.43758695 0.44588515 0.48183223 0.19172186]\n",
      "MV == MVnew:\n",
      "False\n"
     ]
    }
   ],
   "source": [
    "# setup\n",
    "fname = \"test.hdf5\"\n",
    "label = \"my_label\"\n",
    "attrs = {\"butter\": \"jam\"}\n",
    "\n",
    "print(\"Testing serialization:\")\n",
    "MV.save(fname, label, attrs=attrs)\n",
    "MVnew = MV.load(fname, label)\n",
    "\n",
    "print(\"Saved params:\")\n",
    "print(MV.params)\n",
    "print(\"Loaded params:\")\n",
    "print(MVnew.params)\n",
    "\n",
    "print(\"MV == MVnew:\")\n",
    "print(MV == MVnew)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9e7f192a-f75c-4a3e-a4f4-8271fe0a0019",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7ff5866-54d3-4ee1-877d-11b9961766a2",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "gwalk",
   "language": "python",
   "name": "gwalk"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
